# Object detection

# Usage

```
mkdir detection
cd detection
```

## Smarty library

Clone repo:

```
git clone https://dplaton@bitbucket.org/dplaton/old-smarty.git smarty
```

Download data:

```
wget https://bitbucket.org/dplaton/old-smarty/downloads/tiny-yolo.h5 -O smarty/models/data/tiny-yolo.h5
```

## Object detection

Clone repo:

```
git clone https://dplaton@bitbucket.org/dplaton/object_detection.git
```

Run:
```
cd object_detection
./dpq_detection.py
```
