import cv2
import os
import time
from datetime import datetime
from smarty.handlers.yolo_base_handler import YoloBaseHandler
from object_detection.simple_logger import SimpleLogger


class YoloHandler(YoloBaseHandler):
    """Yolo handler class."""

    def __init__(self, process, model, **options):
        super().__init__(process, model, **options)
        self._process_name = self._process.name
        self._model_classes = self._model.CLASSES
        self._object_classes = self._model_classes
        self._model_class_colors = []
        self._init_model_class_colors()
        self._logger = SimpleLogger(options.get('debug', False))
        self._img_dir_path = options.get('img_dir_path', 'img')
        self._input_matrix = None
        self._start_evaluation_time = None

    def _init_model_class_colors(self):
        max_color_value = 256 ** 3 - 1
        interval = int(max_color_value / len(self._model_classes))
        for color_value in range(0, max_color_value, interval):
            blue = color_value & 255
            green = (color_value >> 8) & 255
            red = (color_value >> 16) & 255
            self._model_class_colors.append((blue, green, red))

    def _get_object_color(self, object_class):
        return self._model_class_colors[object_class]

    def _set_model_class_color(self, class_name, color):
        index = self._model_classes.index(class_name)
        self._model_class_colors[index] = color

    def _draw_object(self, obj, box_border_color, box_border_size=2):
        point_1 = (obj['box']['left'], obj['box']['bottom'])
        point_2 = (obj['box']['right'], obj['box']['top'])
        cv2.rectangle(self._input_matrix, point_1, point_2, box_border_color,
                      box_border_size)

    def _save_matrix(self):
        date_format = '%Y-%m-%d %H-%M-%S.%f'
        split_datetime = datetime.now().strftime(date_format).split(' ')
        img_dir_path = os.path.join(self._img_dir_path, split_datetime[0])
        os.makedirs(img_dir_path, exist_ok=True)
        img_name = '{}-{}.png'.format(split_datetime[1], self._process_name)
        img_path = os.path.join(img_dir_path, img_name)
        cv2.imwrite(img_path, self._input_matrix)

    def _debug_log(self, message, *format_args):
        message = '[{}], {}: ' + message
        format_time = datetime.now().strftime('%H:%M:%S')
        format_args = [format_time, self._process_name] + list(format_args)
        self._logger.debug_log(message, *format_args)

    def handle_task(self, input_matrix):
        self._input_matrix = super().handle_task(input_matrix)
        self._start_evaluation_time = time.time()
        return self._input_matrix

    def handle_evaluation(self, objects):
        evaluation_time = time.time() - self._start_evaluation_time
        num_found_objects = 0
        for obj in objects:
            if obj['class_name'] not in self._object_classes:
                continue
            num_found_objects += 1
            object_color = self._get_object_color(obj['class'])
            self._draw_object(obj, object_color)
        if not num_found_objects:
            self._debug_log('evaluation time: {:0.3f}, no objects found',
                            evaluation_time)
            return
        self._debug_log('evaluation time: {:0.3f}, found objects: {}',
                        evaluation_time, num_found_objects)
        self._save_matrix()
