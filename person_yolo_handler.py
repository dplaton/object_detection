from object_detection.yolo_handler import YoloHandler


class PersonYoloHandler(YoloHandler):
    """Person yolo handler class."""

    def __init__(self, process, model, **options):
        super().__init__(process, model, **options)
        self._object_classes = ['person']
        color = options.get('color', (0, 255, 0))
        self._set_model_class_color('person', color)
