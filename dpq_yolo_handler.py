import os
import shutil
import cv2
from datetime import datetime
import time
from object_detection.person_yolo_handler import PersonYoloHandler

HTML_TR_TPL = """<tr>
                <td>{}</td>
                <td>{}</td>
                <td>{:0.3f}</td>
                <td><a href="/{}" target="_blank">show</a></td>
            </tr>
            <!--TR_PLACE_HOLDER-->"""


class DpqYoloHandler(PersonYoloHandler):
    """Dpq yolo handler class."""

    def __init__(self, process, model, **options):
        super().__init__(process, model, **options)
        self._log_file_path = options.get('log_file_path', 'log.txt')
        self._html_dir_path = options.get('html_dir_path', 'html')
        self._html_file_name = options.get('html_file_name', 'index.html')
        self._ensure_index_html()

    def _ensure_index_html(self):
        os.makedirs(self._html_dir_path, exist_ok=True)
        html_file_path = os.path.join(self._html_dir_path, self._html_file_name)
        if not os.path.exists(html_file_path):
            shutil.copy('index.html', html_file_path)

    def _file_log(self, message, *format_args):
        with open(self._log_file_path, 'a') as file:
            print(message.format(*format_args), file=file)

    def _write_html(self, format_time, num_found_objects, evaluation_time,
                    img_name):
        html_file_path = os.path.join(self._html_dir_path, self._html_file_name)
        with open(html_file_path, 'r') as file:
            html = file.read()
        tr_data = HTML_TR_TPL.format(format_time, num_found_objects,
                                     evaluation_time, img_name)
        html = html.replace("<!--TR_PLACE_HOLDER-->", tr_data)
        with open(html_file_path, 'w') as file:
            file.write(html)

    def _save_matrix(self):
        date_format = '%Y-%m-%d %H-%M-%S.%f'
        split_datetime = datetime.now().strftime(date_format).split(' ')
        img_dir_path = os.path.join(self._img_dir_path, split_datetime[0])
        os.makedirs(img_dir_path, exist_ok=True)
        img_name = '{}-{}.png'.format(split_datetime[1], self._process_name)
        img_path = os.path.join(img_dir_path, img_name)
        cv2.imwrite(img_path, self._input_matrix)
        return os.path.join(split_datetime[0], img_name)

    def handle_evaluation(self, objects):
        evaluation_time = time.time() - self._start_evaluation_time
        num_found_objects = 0
        for obj in objects:
            if obj['class_name'] not in self._object_classes:
                continue
            num_found_objects += 1
            object_color = self._get_object_color(obj['class'])
            self._draw_object(obj, object_color)
        if not num_found_objects:
            self._debug_log('evaluation time: {:0.3f}, no objects found',
                            evaluation_time)
            return
        self._debug_log('evaluation time: {:0.3f}, found objects: {}',
                        evaluation_time, num_found_objects)
        img_name = self._save_matrix()
        format_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self._file_log('{}, num: {}, eval time: {:0.3f}', format_time,
                       num_found_objects, evaluation_time)
        self._write_html(format_time, num_found_objects, evaluation_time,
                         img_name)
