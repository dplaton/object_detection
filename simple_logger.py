class SimpleLogger(object):
    """Simple logger class."""

    def __init__(self, debug):
        self._debug = debug

    def debug_log(self, message, *format_args):
        if self._debug:
            print(message.format(*format_args))
