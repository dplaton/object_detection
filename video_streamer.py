import time
import cv2
from datetime import datetime
from smarty.executor import Executor
from object_detection.simple_logger import SimpleLogger


class VideoStreamer(object):
    """Video streamer class."""

    def __init__(self, input_stream, options, executor_options, model,
                 model_options, handler, handler_options):
        self._input_stream = input_stream
        # TODO: use original python logger
        self._logger = SimpleLogger(options.get('debug', False))
        self._max_num_empty = options.get('max_num_empty', 15)
        self._resize = options.get('resize', (0, 0))
        self._rotate = options.get('rotate', 0)
        # TODO: rename to process interval
        self._evaluation_interval = options.get('evaluation_interval', 2.0)
        self._stop_timeout = options.get('stop_timeout', 10)
        self._executor = Executor(executor_options, model, model_options,
                                  handler, handler_options)
        self._capture = None

    def _debug_log(self, message, *format_args):
        message = '[{}], detector: ' + message
        format_time = datetime.now().strftime('%H:%M:%S')
        format_args = [format_time] + list(format_args)
        self._logger.debug_log(message, *format_args)

    def _resize_matrix(self, matrix):
        return cv2.resize(matrix, self._resize)

    def _rotate_matrix(self, matrix):
        # TODO: fix rotate algorithm
        width = matrix.shape[1]
        height = matrix.shape[0]
        rotate_matrix = cv2.getRotationMatrix2D((width/2, height/2),
                                                self._rotate, 1)
        return cv2.warpAffine(matrix, rotate_matrix, (width, height))

    def _executor_stop(self):
        self._executor.stop(self._stop_timeout)

    def _is_empty_matrices(self):
        for _ in range(self._max_num_empty):
            res, matrix = self._capture.read()
            if matrix.shape != (0, 0, 0):
                return False
        return True

    def _process_stream(self):
        evaluation_start_time = 0
        while True:
            res, matrix = self._capture.read()
            cv2.waitKey(30)
            if not res:
                self._debug_log('video streaming finished')
                break
            if matrix.shape == (0, 0, 0):
                if self._is_empty_matrices():
                    message = 'too many empty frames, stop video streaming'
                    self._debug_log(message)
                    break
            if self._resize != (0, 0):
                matrix = self._resize_matrix(matrix)
            if self._rotate:
                matrix = self._rotate_matrix(matrix)
            current_time = time.time()
            if current_time - evaluation_start_time > self._evaluation_interval:
                self._executor.put(matrix)
                evaluation_start_time = current_time
        self._executor_stop()

    def start(self):
        self._capture = cv2.VideoCapture(self._input_stream)
        res, matrix = self._capture.read()
        # TODO: check matrix.shape separately
        assert res or matrix.shape == (0, 0, 0), 'video streaming init error'
        self._debug_log('start video processing, original frame size: {}x{}',
                        matrix.shape[1], matrix.shape[0])
        if self._resize != (0, 0):
            self._debug_log('each frame will be resized: {}x{}',
                            self._resize[0], self._resize[1])
        if self._rotate:
            self._debug_log('each frame will be rotated: {}', self._rotate)
        self._executor.start()
        try:
            self._process_stream()
        except KeyboardInterrupt:
            self._debug_log('Ctrl+C detected, stop video streaming')
            self._executor_stop()
