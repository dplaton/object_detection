#!/usr/bin/python3

import sys
import os
sys.path.append(os.path.dirname(os.getcwd()))
import warnings
import argparse
from smarty.models.yolo_model import YoloModel
from object_detection.person_yolo_handler import PersonYoloHandler
from object_detection.video_streamer import VideoStreamer


os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
warnings.simplefilter('ignore', UserWarning)

parser = argparse.ArgumentParser(description='Person detection.')
parser.add_argument('-is', '--input-stream', required=True,
                    help='input video stream')
# Video streamer options.
parser.add_argument('-d', '--debug', action='store_true', default=False,
                    help='enable debug mode')
parser.add_argument('-mne', '--max-num-empty', type=int, default=15,
                    help='maximum number of empty frames to stop processing')
parser.add_argument('-re', '--resize', type=int, nargs=2, default=[0, 0],
                    help='resize each input frame')
parser.add_argument('-ro', '--rotate', type=int, default=0,
                    help='rotate each input frame')
parser.add_argument('-ei', '--evaluation-interval', type=float, default=2.0,
                    help='evaluation interval')
parser.add_argument('-st', '--stop-timeout', type=int, default=10,
                    help='stop timeout')
# Executor options.
parser.add_argument('-mqs', '--max-queue-size', type=int, default=100,
                    help='maximum size of the queue')
parser.add_argument('-np', '--num-processes', type=int, default=1,
                    help='number of process')
parser.add_argument('-pnp', '--process-name-prefix', default='p-',
                    help='process name prefix')
# Yolo handler options.
parser.add_argument('-idp', '--img-dir-path', default='img',
                    help='image dir path')
# Yolo model options.
parser.add_argument('-s-thr', '--score-threshold', type=float, default=0.1,
                    help='yolo model score threshold')
parser.add_argument('-i-thr', '--iou-threshold', type=float, default=0.4,
                    help='yolo model iou threshold')
args = parser.parse_args()

input_stream = args.input_stream
video_streamer_options = {
    'debug': args.debug,
    'max_num_empty': args.max_num_empty,
    'resize': tuple(args.resize),
    'rotate': args.rotate,
    'evaluation_interval': args.evaluation_interval,
    'stop_timeout': args.stop_timeout
}
executor_options = {
    'max_queue_size': args.max_queue_size,
    'num_processes': args.num_processes,
    'process_name_prefix': args.process_name_prefix
}
yolo_handler_options = {
    'debug': args.debug,
    'img_dir_path': args.img_dir_path
}
yolo_model_options = {
    'score_threshold': args.score_threshold,
    'iou_threshold': args.iou_threshold
}

video_streamer = VideoStreamer(input_stream, video_streamer_options,
                               executor_options, YoloModel, yolo_model_options,
                               PersonYoloHandler, yolo_handler_options)
video_streamer.start()
