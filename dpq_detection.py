#!/usr/bin/python3

import sys
import os
sys.path.append(os.path.dirname(os.getcwd()))
import warnings
from smarty.models.yolo_model import YoloModel
from object_detection.dpq_yolo_handler import DpqYoloHandler
from object_detection.video_streamer import VideoStreamer

input_stream = 'rtsp://192.168.10.10:554/s0'
debug = True
rotate = 90
evaluation_interval = 1.0

max_queue_size = 10
num_processes = 1
process_name_prefix = 'dpq-'

img_dir_path = 'html'
log_file_path = 'html/log.txt'
html_dir_path = 'html'


score_threshold = 0.2
iou_threshold = 0.4

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
warnings.simplefilter('ignore', UserWarning)

video_streamer_options = {
    'debug': debug,
    'rotate': rotate,
    'evaluation_interval': evaluation_interval
}
executor_options = {
    'max_queue_size': max_queue_size,
    'num_processes': num_processes,
    'process_name_prefix': process_name_prefix
}
yolo_handler_options = {
    'debug': debug,
    'img_dir_path': img_dir_path,
    'log_file_path': log_file_path,
    'html_dir_path': html_dir_path
}
yolo_model_options = {
    'score_threshold': score_threshold,
    'iou_threshold': iou_threshold
}

video_streamer = VideoStreamer(input_stream, video_streamer_options,
                               executor_options, YoloModel, yolo_model_options,
                               DpqYoloHandler, yolo_handler_options)
video_streamer.start()
